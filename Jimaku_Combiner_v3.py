import os
import glob
import re

def merge_subs(scan_directory, files_extension, output_filename):
    pass

'''
scan_directory = "J:/Books/Jap/Subtitles/TMP"
merge_subs(scan_directory, "srt", "output.txt)
'''

#сначала обрабатываем srt сабы
buffer = " "
file_list = glob.glob('**/*.srt', recursive=True)

cnt = 0
for file in file_list:
    with open(file, "a+", encoding='UTF-8', errors='ignore') as out_file:
        buffer = out_file.readline()
        pointer = ""
        with open(file, "r", encoding='UTF-8', errors='ignore') as f:
            pointer = re.sub('[0-9 + : + --> +  A-z ]',"", f.read())
            pointer = pointer.replace("\n\n", "")
            buffer += "\n" + str(file) + "\n"
            buffer += pointer
            
            
        print(file_list[cnt])
        cnt += 1

    print("OK")
    with open("Combined_subs.txt", "a+", encoding="utf-8-sig") as f:
        f.write(buffer)

#сначала обрабатываем ass сабы
buffer = " "
file_list = glob.glob('**/*.ass', recursive=True)

cnt = 0
for file in file_list:
    with open(file, "a+", encoding='UTF-8', errors='ignore') as out_file:
        buffer = out_file.readline()
        pointer = ""
        with open(file, "r", encoding='UTF-8', errors='ignore') as f:
            pointer = re.sub('[0-9 + : + --> +  A-z + \{ + \} + ".." ]',"", f.read())
            pointer = pointer.replace("\n\n", "")
            buffer += "\n" + str(file) + "\n"
            buffer += pointer
        print(file_list[cnt])
        cnt += 1

    print("OK")
    with open("Combined_subs.txt", "a+", encoding="utf-8-sig") as f:
        f.write(buffer)
