FROM python:3.8-alpine

RUN adduser -D -H app

WORKDIR /app

RUN apk add --no-cache \
    gcc \
    musl-dev

COPY requirements.txt .

RUN pip install --no-cache-dir -Ur requirements.txt

USER app

COPY . .

ENTRYPOINT ["python", "-m", "sub_combiner"]