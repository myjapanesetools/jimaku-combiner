import argparse
import glob
import pprint
import time

ap = argparse.ArgumentParser()
ap.add_argument("-w", "--word", required=True, help="Word to search")
ap.add_argument("-t", "--translate", required=False, help="Translate with google or deepl")
args = vars(ap.parse_args())

start = time.time()


def clean_text(text: str):
    return text.replace("\n", "").replace("\\N", "")


def search_word(word: str, next_line_num: int = 2, search_limit: int = 5):
    outputs = glob.glob('output/**/*.txt', recursive=True)
    result = {}
    result_count = 0

    for file in outputs:
        with open(file) as f:

            for num, line in enumerate(f, start=1):
                result_text = []

                if result_count >= search_limit:
                    break

                if word in line:
                    result_count += 1
                    # print(line)
                    result_text.append(clean_text(line))
                    for i in range(1, next_line_num):
                        result_text.append(clean_text(next(f)))
                    result[result_count] = " ".join(result_text)

    return result


stop = time.time()
print(f'Search took {stop-start:.1f} seconds')

res = search_word(word=args["word"])

if not args["translate"]:
    pprint.pprint(res)

if args["translate"] == "google":
    from translate import Translator

    res_with_google = {}
    for k, v in res.items():
        translator = Translator(from_lang="ja", to_lang="ru")
        translation = translator.translate(v)
        res_with_google.update({k: [v, translation]})

    pprint.pprint(res_with_google)
