import sqlite3

from sqlalchemy import (create_engine, MetaData, Table, Column, Integer, Text)


# sqlite_filepath = sqlite3.connect('db/subtitles.db')
# ENGINE = create_engine(f"sqlite:///{sqlite_filepath}")

meta = MetaData()

subtitles = Table(
    'subtitles',
    meta,
    Column('id', Integer, primary_key=True),
    Column('sub_text', Text, unique=False),
)

# meta.create_all(bind=ENGINE, tables=[subtitles])


def get_conn(sqlite_path: str):
    engine = create_engine(f"sqlite:///{sqlite_path}")
    return engine.connect()


def initdb(path: str) -> None:
    create_sub_table = """
    CREATE TABLE subtitles(
        id INTEGER NOT NULL PRIMARY KEY,
        sub_text Text
    );
    """

    conn = get_conn(path)
    conn.execute(create_sub_table)
    conn.close()


# initdb("db/subtitles.db")

