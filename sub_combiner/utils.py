import csv
import re


def cjk_detect(texts):
    # korean
    if re.search("[\uac00-\ud7a3]", texts):
        return "ko"
    # japanese
    if re.search("[\u3040-\u30ff]", texts):
        return "ja"
    # chinese
    if re.search("[\u4e00-\u9FFF]", texts):
        return "zh"
    return None


def srt_process_line(line: str):
    if cjk_detect(line) == "ja" or cjk_detect(line) == "zh":
        return line.replace("\n", "").replace("\t", "").strip()


def get_jpline_from_str_file(file_name: str):
    try:
        with open(file_name, "r", encoding='UTF-8') as out_file:
            for num, line in enumerate(out_file, start=1):
                clean_text = srt_process_line(line)
                yield clean_text
    except Exception as er:
        print(er)
        try:
            with open(file_name, "r", encoding='shiftjis') as out_file:
                for line in out_file:
                    clean_text = srt_process_line(line)
                    yield clean_text
        except Exception as er2:
            print(er2)
            yield None


def get_jpline_from_ass_file(file_name: str):
    try:
        with open(file_name, "r", encoding='UTF-8') as out_file:
            for num, line in enumerate(out_file, start=1):

                if "[Events]" in line:
                    reader = csv.DictReader(out_file)
                    for row in reader:
                        # cases: JP-TEXT, CN-TEXT, JP, ''(empty). NTP
                        if "CN-text" not in row[" Style"]:
                            yield srt_process_line(row[" Text"])
    except Exception as er:
        print(er)
        return None
