import typing

from sub_combiner.utils import get_jpline_from_ass_file, get_jpline_from_str_file


def process_one(file: str, file_generator, limit: int, text_count: int, text_list: typing.List[str]):
    for one_line in file_generator(file):
        if one_line:
            text_list.append(one_line)
        if len(text_list) % limit == 0 and len(text_list) != 0:
            with open(f"output/{text_count}-output-{file.split('.')[-1]}.txt", "wt") as output2:
                for one_file in text_list:
                    output2.write(one_file + "\n")
            text_count += 1
            text_list = []
    return text_count, text_list


def process_files(
    # db_conn,
    limit: int,
    file_list: typing.List[str],
) -> None:
    text_list = []
    text_count = 1

    for file in file_list:
        file_ext = file.split(".")[-1]
        print(f"File({file_ext}): {file}")
        if file_ext == "ass":
            text_count, text_list = process_one(
                file=file,
                file_generator=get_jpline_from_ass_file,
                limit=limit,
                text_count=text_count,
                text_list=text_list,
            )
        if file_ext == "srt":
            text_count, text_list = process_one(
                file=file,
                file_generator=get_jpline_from_str_file,
                limit=limit,
                text_count=text_count,
                text_list=text_list,
            )

        if file_ext != "ass" and file_ext != "srt":
            print("Error: UNKNOWN file extension.")

        with open(f"output/{text_count}-output-{file_ext}.txt", "wt") as output2:
            for one_ass in text_list:
                output2.write(one_ass + "\n")
