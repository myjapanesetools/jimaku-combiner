import glob
import time

from sub_combiner.combine import process_files
from sub_combiner.db import get_conn


srt_list = glob.glob('**/*.srt', recursive=True)
ass_list = glob.glob('**/*.ass', recursive=True)

start = time.time()

LIMIT = 50_000
# DB_CONN = get_conn('db/subtitles.db')

process_files(limit=LIMIT, file_list=srt_list)
process_files(limit=LIMIT, file_list=ass_list)

stop = time.time()
print(f'Operation took {stop-start:.1f} seconds')

# DB_CONN.close()
