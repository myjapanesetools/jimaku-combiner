import os
import glob

def merge_subs(scan_directory, files_extension, output_filename):
    pass

'''
scan_directory = "J:/Books/Jap/Subtitles/TMP"
merge_subs(scan_directory, "srt", "output.txt)
'''

#сначала обрабатываем srt сабы
file_content = " "
file_list = glob.glob('**/*.srt', recursive=True)

cnt = 0
for file in file_list:    
    with open(file, "r", encoding='UTF-8', errors='ignore') as f:
        file_content += f.read()
    print(file_list[cnt])
    cnt += 1

#теперь обрабатываем ass сабы        
cnt = 0
file_list = glob.glob('**/*.ass', recursive=True)
for file in file_list:    
    with open(file, "r", encoding='UTF-8', errors='ignore') as f:
        file_content += f.read()
    print(file_list[cnt])
    cnt += 1
print("OK")
with open("Combined_subs.txt", "a+", encoding="utf-8-sig") as f:
    f.write(file_content)
