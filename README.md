# Faith's Program (sub_combiner)

## Run sub_combiner with docker

1. `docker-compose up`
(if some changes was in docker do `docker-compose up --build`)

2. japanese word search (needs output.txt files in output folder)
`python search.py -w 効果`
   
output:
```
Search took 0.0 seconds
{1: 'そんなに効果あんの\u3000その塾 授業は全部パソコン',
 2: '効果のほどは なかなかか…｡ うっ…｡',
 3: 'ﾄﾞｰﾙとの 相乗効果により さらなる力を得る➡',
 4: 'どれほどの効果があるのかと➡ 心配されておいででした｡',
 5: 'ﾄﾞｰﾙとの 相乗効果により さらなる力を得る｡'}
```

3. japanese word search with translation (only google for now)
`python search.py -w 効果 -t google`
   
output:
```
Search took 0.0 seconds
{1: ['そんなに効果あんの\u3000その塾 授業は全部パソコン',
     'Это настолько эффективно, что все уроки неполной школы проводятся на '
     'компьютере.'],
 2: ['効果のほどは なかなかか…｡ うっ…｡', 'Эффект неплохой ... Эээ ...'],
 3: ['ﾄﾞｰﾙとの 相乗効果により さらなる力を得る➡',
     'Получите больше силы за счет синергетических эффектов с куклами ➡'],
 4: ['どれほどの効果があるのかと➡ 心配されておいででした｡',
     'Я волновался, насколько это будет эффективно.'],
 5: ['ﾄﾞｰﾙとの 相乗効果により さらなる力を得る｡',
     'Получите больше силы за счет синергетических эффектов с куклами.']}
```

# Dima's Program versions

- Jimaku_Combiner.py
- Jimaku_Combiner_v2.py
- Jimaku_Combiner_v3.py

# Notes
```
Some random text.日本語
Fix:
Traceback (most recent call last):
  File "J:\Books\Jap\Subtitles\!Kitsune_neko\Jimaku_Combiner_v2.py", line 36, in <module>
    with open(file, "a+", encoding='UTF-8', errors='ignore') as out_file:
PermissionError: [Errno 13] Permission denied: 'Jinrui wa Suitai Shimashita\\[Koeisub][Sengoku_Basara_Two][01-12][x264_AAC][1280x720].jp.ass'
To DO:
-Add RegExp , remove A-z 0-9
-Добавить возможность пути для сканирования и типы файлов.
```